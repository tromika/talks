# Budapest BI 2017 - 15/11/2017


### The evolution of data-driven culture in the online travel

[Presentation](https://docs.google.com/presentation/d/1HP7oA9DxcXv0bqFhlpHS5BIYm59O2IGbWGbhji024z4/edit?usp=sharing)

The term data-driven has become a buzzword but what about the implementation in a company? All should always aim to be more data-driven but road towards to this goal could be rugged. Companies collect some data and this is the first baby step to be data-driven or at least data-informed. If you want to make lives easier and solve problems only that matters how you transform your assets into insights, utilize data or enable better business decisions.
In this talk,  I’m going to show you problems we solved and some details how you can leverage modern data tools.
